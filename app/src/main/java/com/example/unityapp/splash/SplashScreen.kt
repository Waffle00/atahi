package com.example.unityapp.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.unityapp.R
import kotlinx.android.synthetic.main.fragment_splash_screen.*

class SplashScreen : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
    }

    private fun initListener() {
        btn_splash_to_login.setOnClickListener {
            findNavController().navigate(R.id.action_splashScreen_to_loginFragment)
        }

        btn_splash_to_regis.setOnClickListener {
            findNavController().navigate(R.id.action_splashScreen_to_registerFragment)
        }
    }
}
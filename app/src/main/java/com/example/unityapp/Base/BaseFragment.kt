package com.example.unityapp.Base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        onViewReady(savedInstanceState)
        observeData()
    }

    protected abstract fun onViewReady(savedInstanceState: Bundle?)

    protected abstract fun observeData()

    protected fun hideToolbar(isHide: Boolean = false) {
        (activity as AppCompatActivity).supportActionBar?.let {
            if (isHide) {
                it.hide()
            } else {
                it.show()
            }
        }
    }
}
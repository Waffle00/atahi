package com.example.unityapp.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.example.unityapp.R
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun setupToolbarProperties(
        toolbarId: Toolbar,
        title: String,
        @DrawableRes drawable: Int = R.drawable.ic_back
    ) {
        (activity as AppCompatActivity).run {
            setSupportActionBar(toolbarId)
            supportActionBar?.let {
                toolbarId.title = title
                toolbarId.setTitleTextColor(
                    resources.getColor(R.color.white)
                )
                it.setDisplayHomeAsUpEnabled(true)
                it.setDisplayShowHomeEnabled(true)
                it.setHomeAsUpIndicator(drawable)
            }
        }
    }
}